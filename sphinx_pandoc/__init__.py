# coding: utf8
from docutils import nodes
from docutils.parsers.rst import Directive
from sphinx.locale import _
from docutils.parsers.rst import directives
from sphinx.errors import SphinxError
import subprocess
import os


class PandocNode(nodes.Element):
    pass



class PandocDirective(Directive):

    has_content = True
    required_arguments = 1
    optional_arguments = 0

    option_spec = {
        "options": directives.unchanged,
    }

    def run(self):
        env = self.state.document.settings.env
        pandoc = PandocNode()
        (relative_filename, absolute_filename) = env.relfn2path(
            self.arguments[0])
        pandoc["fichier_source"]=absolute_filename
        pandoc["fichier_rel"]=relative_filename
        pandoc["options"] = self.options["options"]
        return [pandoc]


 
def generate_names(self, node):
    return (os.path.join(self.builder.outdir, node["fichier_rel"]+".html"),  node["fichier_rel"]+".html")

def render_pandoc(self,node):
    (name, rel_name)=generate_names(self,node)
    try:
        os.makedirs(os.path.dirname(name), exist_ok=True)
        try:
            options = ["pandoc"] + node["options"].split()+[node["fichier_source"]] + ["-o",name]
            p = subprocess.Popen(options,
                                 stdout=subprocess.PIPE, stdin=subprocess.PIPE,
                                 stderr=subprocess.PIPE)
        except OSError as err:
            if err.errno != ENOENT:
                raise
            raise SphinxError('pandoc cannot be run')
        sstd,serr = p.communicate()
        if p.returncode != 0:
            raise SphinxError('error while running pandoc\n\n%s' % serr)
        return rel_name
    finally:
        pass

def unsupported_visit_pandoc(self, node):
    self.builder.warn('pandoc: unsupported output format (node skipped)')
    raise nodes.SkipNode

def html_visit_plantuml(self, node):
    nom_fichier = render_pandoc(self,node)
    self.body.append("<div><a href='%s'> Présentation </a><iframe src=%s></iframe></div>"%(nom_fichier,nom_fichier))





def depart_pandoc_node(self, node):
    pass


def latex_departure(self, node):
    pass


def visit_latex(self, node):
    pass





_NODE_VISITORS = {
    'html': (html_visit_plantuml, depart_pandoc_node),
    'latex': (unsupported_visit_pandoc, None), # TODO
    'man': (unsupported_visit_pandoc, None),  # TODO
    'texinfo': (unsupported_visit_pandoc, None),  # TODO
    'text': (unsupported_visit_pandoc, None),
}

def setup(app):
    app.add_node(PandocNode, **_NODE_VISITORS)
    app.add_directive('pandoc', PandocDirective)

    return {'version': '0.1'}   # identifies the version of our extension

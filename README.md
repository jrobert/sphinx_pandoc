This extension is intended to embed pandoc generated slideshow in a sphinx documentation.

Example of usage :

with a file named example.md containing the presentation, use the directive in your rst :


.. pandoc:: example.md
   :options: -t revealjs -V theme=white -V revealjs-url=http://lab.hakim.se/reveal-js -s



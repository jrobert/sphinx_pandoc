.. sphinx-pandoc documentation master file, created by
   sphinx-quickstart on Thu Nov 22 14:21:12 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to sphinx-pandoc's documentation!
=========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:




.. pandoc:: exemple.md
   :options: -t revealjs -V theme=white -V revealjs-url=http://lab.hakim.se/reveal-js -V showNotes=true -s



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

% Exemple de presentation

---
<style type="text/css">
  .reveal p {
    text-align: left;
  }
  .reveal ul {
    display: block;
  }
  .reveal ol {
    display: block;
  }
</style>

# Titre 1

## Sous tritre1 

- item 1

- item 2

- item 3


::: notes

* ce sont mes notes

:::

## Autre slide

![](images/image.png){ width=40%%}
---------

Pas de titre !
----------


